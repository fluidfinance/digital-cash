const { ethers, upgrades } = require('hardhat');

const readline = require('readline');

async function main() {

  let offchainTreasuryOracleAddress = process.env.OFFCHAIN_TREASURY_ADDRESS

  if(!offchainTreasuryOracleAddress) {
    throw('env OFFCHAIN_TREASURY_ADDRESS needs to be set')
  }

  const RWATFactory = await ethers.getContractFactory('RealWorldAssetTether');
  console.log('Deploying upgradable rwat with');
  console.log(`OFFCHAIN_TREASURY_ADDRESS: ${offchainTreasuryOracleAddress}`)
  
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

  rl.question('Proceed with these settings? (y/N): ', async (answer) => {
    if(answer != 'y' && answer != 'Y') {
      console.error('aborted by user')
      process.exit(1)
    }

    console.log('deploying contract...')

    const dusd = await upgrades.deployProxy(
      RWATFactory, 
      [
        'DigitalDollar', 
        'DUSD', 
        offchainTreasuryOracleAddress, 
        '0x...' // whitelist signer
      ], 
    );
    
    await dusd.deployed();
    console.log(`digital currency contract deployed to: `, dusd.address);
  
    rl.close();
  });

}

try {
  main()
}
catch(error) {
  console.error(error);
  process.exit(1);
}