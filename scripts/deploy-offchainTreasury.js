const { ethers } = require('hardhat');

async function main() {
  const OffchainTreasury = await ethers.getContractFactory('OffchainTreasuryOracle');
  console.log('Deploying OffchainTreasury.');

  const offchainSigner = '0x...' // address of the offchain signer

  const offchainTreasury = await OffchainTreasury.deploy(offchainSigner); 
  console.log(`offchainTreasury deployed to: `, offchainTreasury.address);
  console.log(`with offchainSigner:          `, offchainSigner);
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });