# Digital Cash

Digital cash is a digital representation of world currencies that can be used in the digital world (e.g. Metaverse, NFTs, DeFi). It is not a traditional crypto currency or "stablecoin" but simply a representation of traditional money, a "synthetic".  This traditional money always remains in a bank account with our banking partners; it does not move between the traditional world and crypto exchanges.

## Minting and Redeeming process

### High Level Overview
#### Mint
1. User moves funds from his bank account to the DUSD treasury bank account
2. A validator node monitors the DUSD treasury account, sees the new inflow and updates the OffchainTreasuryOracle 
3. A corresponding amount of DUSD token is minted to the user's wallet

#### Redeem
1. User calls the `redeem` function in the DUSD smart contract
2. The contract burns the specified amount of token
3. A validator node monitors the contract for the `Redeem` event and moves a corresponding amount of USD from the DUSD treasury bank account to the user's bank account.

<img src="img/mint-redeem-flow.jpeg" alt="Minting and redeeming process" width="900px" />

### Detailed Mint/Redeem Description

Involved contracts: 
* `RealWorldAssetTether.sol` 
* `OffchainTreasuryOracle.sol`

`RealWorldAssetTether` is the generalized contract and an instance will be deployed as `DUSD`.

#### Mint
1. The validator node picks up a new deposit in the treasury bank account
```javascript
{
  amount: 1000.00,  // amount of USD deposited
  address: '0x123', // user has proven ownership of this wallet
  chain: 'ETHEREUM', // the chain the user wants to receive the tokens on
  txID: '111-222-333-444-1', // CeFi transaction ID
  // solidity compatible ECDSA signature over the above 4 values signed with the CeFi partners private key
  signature: '9cb87877a736922a6c7a8f6ef5c3ec32536835e738683daa0b300fcd2a447ebf09c0f4ec144361d713ece0a81230ac3bd161420fe1c95c9bc4376a02329474751b', 
},
```

2. The validator node calls `update` on the `OffchainTreasuryOracle` contract
```solidity
function update(
    int256[] calldata _amounts, 
    address[] calldata _addresses, 
    bytes32[] calldata _cefiTxIDs,
    uint256[] calldata _chainIDs,
    bytes[] calldata _signatures
  ) external onlyRole(ORACLE_UPDATER_ROLE)
```

3. The OffchainTreasuryOracle contract checks the signature and ensures it was signed by the `offchainSigner` address
4. The OffchainTreasuryOracle contract updates the internal `balance` and calls `mint` in the associated `tokenContract` (DUSD)
5. The `tokenContract` checks collateralization
```solidity
require(
  (totalSupply() + totalPendingRedeems + _amount) <= offchainTreasuryOracle.getLatestAnswer(),
  "totalSupply cannot be greater than offchain total supply"
);
```
6. The `tokenContract` mints the tokens to the user's associated wallet.


#### Redeem
1. A user calls `redeem` on the `RealWorldAssetTether` (DUSD) contract 
2. The contract burns the tokens 
3. The contract keeps track of the `totalPendingRedeems` for the collateralization check
4. The contract emits a `Redeem` event
5. The validator node picks up the `Redeem` event and transfers a corresponding amount of USD from the offchain treasury bank account to the users bank account.
6. The validator node calls `update` on the `OffchainTreasuryOracle` contract with the new outflow transaction
7. The `update` call is validated and calls `releasePendingRedeem` on the `tokenContract` (DUSD)
8. `releasePendingRedeem` reduces `totalPendingRedeems` so the `totalSupply + totalPendingRedeems` is in sync with the `OffchainTreasuryOracle.balance`

## Testing and Development

Contracts are built on top of `@openzeppelin/contracts` and `@openzeppelin/contracts-upgradeable` using hardhat and ethereum-waffle.

To test or develop checkout the repo and run
```
npm install
```


# Deployment

## OffchainTreasuryOracle
```
$ npx hardhat run --network arbitrumtest scripts/deploy-offchainTreasury.js 
Deploying OffchainTreasury.
offchainTreasury deployed to:  0x123...
with offchainSigner:           0x...
```


## DUSD
```
$ export OFFCHAIN_TREASURY_ADDRESS=0x123
$ npx hardhat run --network arbitrumtest scripts/deploy-DUSD.js
Deploying DUSD with
OFFCHAIN_TREASURY_ADDRESS: 0x123
Proceed with these settings? (y/N): y
deploying contract...
digital currency contract deployed to:  0x...
```

## Final Configuration
Set DUSD contract in offchainTreasuryOracle:
```
await offchainTreasuryOracle.updateTokenContract('0x...')
```

Give node address ORACLE_UPDATER_ROLE in offchainTreasuryOracle
```
await offchainTreasuryOracle.grantRole(ORACLE_UPDATER_ROLE, '0x...')
```


## Deploy other Digital Currency
Same as above, just edit the constructor parameters in `deploy-DUSD.js` L:33 and L:34