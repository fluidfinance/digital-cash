// SPDX-License-Identifier: MIT
pragma solidity 0.8.9;

import "../oracles/OffchainTreasuryOracle.sol";

/**
  @dev Mock contract for easier testing.
*/
contract MockOffchainTreasuryOracle is OffchainTreasuryOracle {

  constructor() OffchainTreasuryOracle(address(0x90F79bf6EB2c4f870365E785982E1f101E93b906)) {
  }

  // doesn't require valid signatures
  function updateUnchecked(
    int256 _amount,
    address _address,
    bytes32 _txID
  ) external onlyRole(ORACLE_UPDATER_ROLE) {
   
    balance += _amount;

    if(_amount > 0) {
      tokenContract.mint(_address, uint256(_amount), _txID);
      emit Inflow(_txID, uint256(_amount), 1);
    } else {
      tokenContract.releasePendingRedeem(_address, uint256(_amount), _txID);
      emit Outflow(_txID, uint256(_amount), 1);
    }
  }

  // allows calling mint directly for testing
  function passthroughMint(uint256 _amount, address _to, bytes32 _ceFiTxID) public {
    tokenContract.mint(_to, _amount, _ceFiTxID);
  }

  // sets balance directly
  function setBalance(int256 _balance) public {
    balance = _balance;
  }
}