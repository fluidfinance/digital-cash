const { expect } = require('chai');

const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'
const REVERT_ONLY_WHITELISTED_ADDRESSES = 'only allowed for whitelisted address'
const REVERT_ADDRESS_ALREADY_WHITELISTED = 'address already whitelisted'
const REVERT_ADDRESS_IS_NOT_WHITELISTED = 'already removed from whitelist'
const REVERT_ZERO_ADDRESS_NOT_WHITELISTABLE = 'zero address not allowed to be whitelisted'
const REVERT_BURN_EXCEEDS_BALANCE = 'burn amount exceeds balance'
const REVERT_SUPPLY_CANNOT_EXCEED_ASSETS = 'totalSupply cannot be greater than offchain total supply'
const REVERT_ADDR1_NOT_WHITELISTER_ROLE = 'AccessControl: account 0x70997970c51812dc3a010c7d01b50e0d17dc79c8 is missing role 0x8619cecd8b9e095ab43867f5b69d492180450fe862e6b50bfbfb24b75dd84c8a'
const REVERT_ADDR1_NOT_ORACLE_UPDATER_ROLE = 'AccessControl: account 0x70997970c51812dc3a010c7d01b50e0d17dc79c8 is missing role 0x9792fdc19ab98adfa72ab2fa98d342618c661e01c406979c105b31eda87f5e6f'
const REVERT_INVALID_UPDATE_ARGUMENTS = 'invalid arguments: same number of amounts, addresses, cefiTxIDs and signatures must be specified'
const REVERT_MINT_CALL_NONTREASURY = 'caller is not treasury'
const REVERT_RELEASEREDEEM_CALL_NONTREASURY = 'caller is not treasury'
const REVERT_INITIALIZE_TREASURY_ADDRESSZERO = '_offchainTreasuryOracle is 0x0';
const REVERT_PAUSE_ALREADYPAUSED = 'paused'
const REVERT_UNPAUSE_NOTPAUSED = 'not paused'
const REVERT_PAUSING_MISSINGROLE = 'AccessControl: account 0x3c44cdddb6a900fa2b585dd299e03d12fa4293bc is missing role 0x65d7a28e3265b37a6474929f336521b332c1681b933f6cb9f3376673440d862a'
const REVERT_TOKEN_CONTRACT_ZERO_ADDRESS = '_tokenContract is 0x0'
const REVERT_OFFCHAIN_SIGNER_ZERO_ADDRESS = '_offchainSigner is 0x0'
const REVERT_OFFCHAIN_SIGNER_EQUALS_CURRENT = '_offchainSigner equals current'
const REVERT_MINIMUM_REDEEM_AMOUNT = 'cannot redeem less than minRedeemAmount'
const REVERT_MISSING_ROLE_REDEEM_ADMIN = 'AccessControl: account 0x70997970c51812dc3a010c7d01b50e0d17dc79c8 is missing role 0x281081d9b36b37208f0d8dfce5adc7e00d31ece09269aaa8d0bfa43e6840a338'
const REVERT_MISSING_ROLE_FEE_ADMIN = 'AccessControl: account 0x70997970c51812dc3a010c7d01b50e0d17dc79c8 is missing role 0x349eec46d2c564a125684d1934a6120a70cc341d3a26d362e358a775e07dec17'
const REVERT_MISSING_ROLE_WHITELISTER_ADMIN = 'AccessControl: account 0x70997970c51812dc3a010c7d01b50e0d17dc79c8 is missing role 0xed83bddd166f4b9fbcaab0d32ee668d267bbc0a6c7a28803312393acb6b12306'
const REVERT_SAME_FEE_RECEIVER = '_feeReceiver equals current'
const REVERT_ZERO_ADDRESS_CANNOT_BE_WHITELIST_SIGNER = 'zero address cannot be whitelistSigner'
const REVERT_INVALID_BATCH_WHITELIST_ARGUMENTS = 'invalid arguments: same number of addresses and signatures required'
const REVERT_ADDRESS_BLACKLISTED = 'address is blacklisted'
  
const CHAIN_ID_ETHEREUM = 31337 // hardhat uses this
const CHAIN_ID_POLYGON = 137 // https://magic.link/docs/blockchains/polygon
const CEFI_TX_ID01 = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('TX-ID-01'))
const CEFI_TX_ID02 = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('TX-ID-02'))
const CEFI_TX_ID03 = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('TX-ID-03'))
const CEFI_TX_ID04 = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('TX-ID-04'))
const DEFI_TX_ID01 = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('TX-ID-05'))

const RWAT_DECIMALS = 6

const MINTER_ROLE = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('MINTER_ROLE'))
const PAUSER_ROLE = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('PAUSER_ROLE'))
const ORACLE_UPDATER_ROLE = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('ORACLE_UPDATER_ROLE'))
const WHITELISTER_ROLE = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('WHITELISTER_ROLE'))
const WHITELISTER_ADMIN_ROLE = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('WHITELISTER_ADMIN_ROLE'))
const REDEEM_ADMIN_ROLE = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('REDEEM_ADMIN_ROLE'))
const FEE_ADMIN_ROLE = ethers.utils.keccak256(ethers.utils.toUtf8Bytes('FEE_ADMIN_ROLE'))
const ADDRESS_ZERO = '0x0000000000000000000000000000000000000000'
      
const mineBlockAt = async timestamp => {
  if (typeof timestamp !== 'string') timestamp = timestamp.toString();
  timestamp = parseInt(timestamp, 10);
  await ethers.provider.send('evm_setNextBlockTimestamp', [timestamp]);
  await ethers.provider.send('evm_mine');
};

const signUpdateCall = async (signer, amount, address, txId, chainID) => {
  amount = amount < 0 ? -amount : amount;
  return await signer.signMessage(ethers.utils.arrayify(ethers.utils.solidityKeccak256(
    ['uint256','address','bytes32','uint256'],
    [amount, address, txId, chainID]
  )))
}

const signWhitelistAddress = async (signer, address) => {
  return await signer.signMessage(ethers.utils.arrayify(ethers.utils.solidityKeccak256(
    ['address'],
    [address]
  )))
}

const randomBytes32 = () => {
  return ethers.utils.keccak256(ethers.utils.toUtf8Bytes((Math.random() + 1).toString(36)))
}

describe('RWAT', function() {

  let RWATFactory
  let OffchainTreasuryFactory
  let offchainTreasury
  let rwat
  let owner
  let addr1
  let addr2
  let whitelisted
  let NOTwhitelisted
  let addrs

  let mockOffchainTreasuryOracle

  before(async function() {
    RWATFactory = await ethers.getContractFactory('RealWorldAssetTether')
    OffchainTreasuryFactory = await ethers.getContractFactory('OffchainTreasuryOracle')
    MockOffchainTreasuryFactory = await ethers.getContractFactory('MockOffchainTreasuryOracle')
  })

  beforeEach(async function() {
    [owner, addr1, addr2, whitelisted, NOTwhitelisted, ...addrs] = await ethers.getSigners()

    offchainTreasury = await OffchainTreasuryFactory.deploy(owner.address)
    await offchainTreasury.grantRole(ORACLE_UPDATER_ROLE, owner.address)
    
    rwat = await RWATFactory.deploy()
    await rwat.deployed()
    await rwat.initialize(
      'Fully Insured Asset Tethered USD', 
      'rwat', 
      offchainTreasury.address, 
      owner.address
    )
    await rwat.grantRole(MINTER_ROLE, owner.address)
    await rwat.grantRole(WHITELISTER_ROLE, owner.address)
    await rwat.grantRole(WHITELISTER_ADMIN_ROLE, owner.address)
    await rwat.grantRole(ORACLE_UPDATER_ROLE, owner.address)
    await rwat.grantRole(PAUSER_ROLE, owner.address)
    
    await rwat.addToWhitelist(whitelisted.address, signWhitelistAddress(owner, whitelisted.address))
    await rwat.updateMinRedeemAmount(1) // to avoid having to deal with *10**decimals in tests
    await rwat.updateDustDecimals(0) // to avoid dealing with dust in regular tests
    
    await offchainTreasury.updateTokenContract(rwat.address)

    mockOffchainTreasuryOracle = await MockOffchainTreasuryFactory.deploy()
    await mockOffchainTreasuryOracle.updateTokenContract(rwat.address)
  })

  describe('Token Specs', async function() {
    specify('should have correct decimals', async function() {
      const decimals = await rwat.decimals()
      expect(decimals).to.equal(RWAT_DECIMALS)
    })
  })

  describe('Deployment', async function() {
    specify('reverts if offchainTreasuryOracle is address zero', async () => {
      const rwat = await RWATFactory.deploy()
      await rwat.deployed()
      const initializeCall = rwat.initialize(
        'Fully Insured Asset Tethered USD', 
        'rwat', 
        ADDRESS_ZERO,
        owner.address
      )
      await expect(initializeCall).to.be.revertedWith(REVERT_INITIALIZE_TREASURY_ADDRESSZERO)      
    })
    specify('should NOT have an initial total supply', async function() {
      const totalSupply = await rwat.totalSupply()
      expect(totalSupply).to.equal(0)
    })
    specify('should NOT set an initial balance for owner', async function() {
      const ownerBalance = await rwat.balanceOf(owner.address)
      expect(ownerBalance).to.equal(0)
    })
  })
  
  describe('Pausing', async function() {
    specify('pausing reverts if called by account without PAUSER_ROLE', async function() {
      const pauseCall = rwat.connect(addr2).pause()
      await expect(pauseCall).to.be.revertedWith(REVERT_PAUSING_MISSINGROLE)
    })
    specify('unpausing reverts if called by account without PAUSER_ROLE', async function() {
      const pauseCall = rwat.connect(addr2).pause()
      await expect(pauseCall).to.be.revertedWith(REVERT_PAUSING_MISSINGROLE)
    })
    specify('pausing reverts if already paused', async function() {
      await rwat.pause()
      const pauseCall = rwat.pause()
      await expect(pauseCall).to.be.revertedWith(REVERT_PAUSE_ALREADYPAUSED)
    })
    specify('unpausing reverts if not paused', async function() {
      const unpauseCall = rwat.unpause()
      await expect(unpauseCall).to.be.revertedWith(REVERT_UNPAUSE_NOTPAUSED)
    })
    specify('pausing emits Paused event and sets paused to true', async function() {
      const pauseCall = rwat.pause()
      await expect(pauseCall).to.emit(rwat, 'Paused')
      expect(await rwat.paused()).to.equal(true);
    })
    specify('unpausing emits Unpaused event and sets paused to false', async function() {
      await rwat.pause()
      const unpauseCall = rwat.unpause()
      await expect(unpauseCall).to.emit(rwat, 'Unpaused')
      expect(await rwat.paused()).to.equal(false);
    })
  });
  
  describe('Whitelisting', async function() {
    let addr1Sig
    beforeEach(async function() {
      addr1Sig = signWhitelistAddress(owner, addr1.address)
    })
    describe('adding an address to whitelist', async function() {
      specify('providing a valid signature succeeds', async function() {
        const whitelistSigner = await rwat.whitelistSigner()
        expect(whitelistSigner).to.equal(owner.address)

        const whitelisterCall = rwat.addToWhitelist(addr1.address, signWhitelistAddress(owner, addr1.address))
        await expect(whitelisterCall).to.not.be.reverted
      })
      specify('providing an INVALID signature reverts', async function() {
        const nonSigner = addr1
        const whitelistSigner = await rwat.whitelistSigner()
        expect(whitelistSigner).to.not.equal(nonSigner.address)

        const NONsignerCall = rwat.addToWhitelist(addr1.address, signWhitelistAddress(nonSigner, addr1.address))
        await expect(NONsignerCall).to.be.reverted
      })
      specify('whitelisting an address adds it to the whitelist', async function() {
        await rwat.addToWhitelist(addr1.address, addr1Sig)
        const isWhitelisted = await rwat.whitelist(addr1.address)
        expect(isWhitelisted).to.be.true
      })
      specify('emits an event when an address is added to the whitelist', async function() {
        const whitelistCall = rwat.addToWhitelist(addr1.address, addr1Sig)
        await expect(whitelistCall).to.emit(rwat, 'WhitelistAdded').withArgs(addr1.address)
      })
      specify('reverts when trying to whitelist an already whitelisted address', async function() {
        await rwat.addToWhitelist(addr1.address, addr1Sig)
        const whitelistSameAddressAgainCall = rwat.addToWhitelist(addr1.address, addr1Sig)
        await expect(whitelistSameAddressAgainCall).to.be.revertedWith(REVERT_ADDRESS_ALREADY_WHITELISTED)
      })
      specify('reverts when trying to whitelist the zero address', async function() {
        const whitelistZeroAddressCall = rwat.addToWhitelist(ZERO_ADDRESS, signWhitelistAddress(owner, ZERO_ADDRESS))
        await expect(whitelistZeroAddressCall).to.be.revertedWith(REVERT_ZERO_ADDRESS_NOT_WHITELISTABLE)
      })
      specify('reverts when trying to whitelist an address that was previously removed', async function() {
        await rwat.removeFromWhitelist(addr1.address)
        const whitelistCall = rwat.addToWhitelist(addr1.address, addr1Sig)
        await expect(whitelistCall).to.be.revertedWith(REVERT_ADDRESS_BLACKLISTED)
      })
    })
    describe('removing an address from whitelist', async function() {
      specify('WHITELISTER_ROLE is allowed to call removeFromWhitelist', async function() {
        const whitelister = owner
        const hasWhitelisterRole = await rwat.hasRole(WHITELISTER_ROLE, whitelister.address)
        expect(hasWhitelisterRole).to.be.true

        const whitelisterCall = rwat.connect(whitelister).removeFromWhitelist(whitelisted.address)
        await expect(whitelisterCall).to.not.be.reverted
      })
      specify('NON WHITELISTER_ROLE is NOT allowed to call removeFromWhitelist', async function() {
        const nonWhitelister = addr1
        const hasWhitelisterRole = await rwat.hasRole(WHITELISTER_ROLE, nonWhitelister.address)
        expect(hasWhitelisterRole).to.be.false

        const NONwhitelisterCall = rwat.connect(nonWhitelister).removeFromWhitelist(whitelisted.address)
        await expect(NONwhitelisterCall).to.be.reverted
      })
      specify('calling removeFromWhitelist removes the address from the whitelist', async function() {
        const isWhitelistedBefore = await rwat.whitelist(whitelisted.address)
        expect(isWhitelistedBefore).to.be.true
        
        await rwat.removeFromWhitelist(whitelisted.address)

        const isWhitelistedAfter = await rwat.whitelist(whitelisted.address)
        expect(isWhitelistedAfter).to.be.false
      })
      specify('emits an event when an address is removed from the whitelist', async function() {
        const removeFromWhitelistCall = rwat.removeFromWhitelist(whitelisted.address)
        await expect(removeFromWhitelistCall).to.emit(rwat, 'WhitelistRemoved').withArgs(whitelisted.address)
      })
      specify('blacklists the address when trying to remove an address that is not whitlisted', async function() {
        const removeAddressThatIsntWhitelistedCall = rwat.removeFromWhitelist(addr1.address)
        await expect(removeAddressThatIsntWhitelistedCall).to.emit(rwat, 'BlacklistAdded').withArgs(addr1.address)
      })
      specify('blacklists and removes an address from the whitelist when trying to remove an address that is whitlisted', async function() {
        await rwat.addToWhitelist(addr1.address, signWhitelistAddress(owner, addr1.address))
        const removeAddressThatIsntWhitelistedCall = rwat.removeFromWhitelist(addr1.address)
        await expect(removeAddressThatIsntWhitelistedCall).to.emit(rwat, 'WhitelistRemoved').withArgs(addr1.address)
        await expect(removeAddressThatIsntWhitelistedCall).to.emit(rwat, 'BlacklistAdded').withArgs(addr1.address)
      })
      specify('reverts whe trying to remove an address from the whitelist that was already removed', async function() {
        await rwat.addToWhitelist(addr1.address, signWhitelistAddress(owner, addr1.address))
        await rwat.removeFromWhitelist(addr1.address)
        const removeAddressThatIsntWhitelistedCall = rwat.removeFromWhitelist(addr1.address)
        await expect(removeAddressThatIsntWhitelistedCall).to.be.revertedWith(REVERT_ADDRESS_IS_NOT_WHITELISTED)
      })
    })
    describe('batch whitelisting', async function() {
      specify('multiple addresses can be whitelisted in one transaction', async function() {
        const addr1isWhitelistedBefore = await rwat.whitelist(addr1.address)
        expect(addr1isWhitelistedBefore).to.be.false
        const addr2isWhitelistedBefore = await rwat.whitelist(addr2.address)
        expect(addr2isWhitelistedBefore).to.be.false
        
        await rwat.batchAddToWhitelist(
          [addr1.address, addr2.address],
          [signWhitelistAddress(owner, addr1.address), signWhitelistAddress(owner, addr2.address)]
        )
        
        const addr1isWhitelistedAfter = await rwat.whitelist(addr1.address)
        expect(addr1isWhitelistedAfter).to.be.true
        const addr2isWhitelistedAfter = await rwat.whitelist(addr2.address)
        expect(addr2isWhitelistedAfter).to.be.true
      })
      specify('reverts when invalid arguments are passed', async function() {
        const invalidAddressesCall = rwat.batchAddToWhitelist(
          [addr1.address],
          [signWhitelistAddress(owner, addr1.address), signWhitelistAddress(owner, addr2.address)]
        )
        await expect(invalidAddressesCall).to.be.revertedWith(REVERT_INVALID_BATCH_WHITELIST_ARGUMENTS)

        const invalidSignaturesCall = rwat.batchAddToWhitelist(
          [addr1.address, addr2.address],
          [signWhitelistAddress(owner, addr1.address)]
        )
        await expect(invalidSignaturesCall).to.be.revertedWith(REVERT_INVALID_BATCH_WHITELIST_ARGUMENTS)
      })
    })
    describe('updating whitelistSigner', async function() {
      specify('WHITELISTER_ADMIN_ROLE can update whitelistSigner', async function() {
        const isWhitelisterAdmin = await rwat.hasRole(WHITELISTER_ADMIN_ROLE, owner.address)
        expect(isWhitelisterAdmin).to.be.true

        const before = await rwat.whitelistSigner()

        const newSigner = addr1.address
        expect(before).to.not.equal(newSigner)

        const updateCall = rwat.updateWhitelistSigner(newSigner)
        await expect(updateCall).to.not.be.reverted

        const after = await rwat.whitelistSigner()
        expect(after).to.equal(newSigner)
      })
      specify('NON WHITELISTER_ADMIN_ROLE cannot update whitelistSigner', async function() {
        const nonAdmin = addr1
        const isWhitelisterAdmin = await rwat.hasRole(WHITELISTER_ADMIN_ROLE, nonAdmin.address)
        expect(isWhitelisterAdmin).to.be.false

        const updateCall = rwat.connect(nonAdmin).updateWhitelistSigner(addr2.address)
        await expect(updateCall).to.be.revertedWith(REVERT_MISSING_ROLE_WHITELISTER_ADMIN)
      })
      specify('cannot set address(0) as whitelistSigner', async function() {
        const updateCall = rwat.updateWhitelistSigner(ADDRESS_ZERO)
        await expect(updateCall).to.be.revertedWith(REVERT_ZERO_ADDRESS_CANNOT_BE_WHITELIST_SIGNER)
      })
      specify('reverts if new address equals current address', async function() {
        const updateCall = rwat.updateWhitelistSigner(owner.address)
        await expect(updateCall).to.be.revertedWith('_whitelistSigner equals current')
      })
    })
  })

  describe('Redemption', async function() {
    const TEST_BALANCE = 100*10**RWAT_DECIMALS
    
    beforeEach(async function() {
      await rwat.updateMinRedeemAmount(1*10**RWAT_DECIMALS) // $1.00 USD
      await rwat.updateDustDecimals(4)                      // < $0.01 == dust
      await rwat.updateOffchainTreasury(mockOffchainTreasuryOracle.address)
      await mockOffchainTreasuryOracle.updateUnchecked(TEST_BALANCE, whitelisted.address, CEFI_TX_ID01)
    })
    specify('whitelisted address is allowed to redeem tokens', async function() {
      const balance = await rwat.balanceOf(whitelisted.address)
      expect(balance).to.equal(TEST_BALANCE)
      const isWhitelisted = await rwat.whitelist(whitelisted.address)
      expect(isWhitelisted).to.be.true

      const whitelistedRedeemCall = rwat.connect(whitelisted).redeem(TEST_BALANCE)
      await expect(whitelistedRedeemCall).to.not.be.reverted
    })
    specify('NON whitelisted address is NOT allowed to redeem tokens', async function() {
      const NONwhitelisted = addr1
      await rwat.connect(whitelisted).transfer(NONwhitelisted.address, TEST_BALANCE)

      const balance = await rwat.balanceOf(NONwhitelisted.address)
      expect(balance).to.equal(TEST_BALANCE)

      const isWhitelisted = await rwat.whitelist(NONwhitelisted.address)
      expect(isWhitelisted).to.be.false

      const NONwhitelistedRedeemCall = rwat.connect(NONwhitelisted).redeem(TEST_BALANCE)
      await expect(NONwhitelistedRedeemCall).to.be.revertedWith(REVERT_ONLY_WHITELISTED_ADDRESSES)
    })
    specify('you cannot redeem more tokens than you own', async function() {
      const balance = await rwat.balanceOf(whitelisted.address)
      expect(balance).to.equal(TEST_BALANCE)
      
      const invalidRedeemCall = rwat.connect(whitelisted).redeem(TEST_BALANCE+1*10**RWAT_DECIMALS)
      await expect(invalidRedeemCall).to.be.revertedWith(REVERT_BURN_EXCEEDS_BALANCE)
    })
    specify('redeeming tokens emits a Redeem event with address and amount', async function() {
      const balance = await rwat.balanceOf(whitelisted.address)
      expect(balance).to.equal(TEST_BALANCE)

      const validRedeemCall = rwat.connect(whitelisted).redeem(TEST_BALANCE)
      await expect(validRedeemCall).to.emit(rwat, 'Redeem').withArgs(whitelisted.address, TEST_BALANCE)
    })
    specify('redeeming tokens reduces account balance', async function() {
      const balanceBefore = await rwat.balanceOf(whitelisted.address)
      expect(balanceBefore).to.equal(TEST_BALANCE)

      const redeemAmount = TEST_BALANCE - 1*10**RWAT_DECIMALS
      await rwat.connect(whitelisted).redeem(redeemAmount)

      const balanceAfter = await rwat.balanceOf(whitelisted.address)
      expect(balanceAfter).to.equal(TEST_BALANCE - redeemAmount)
    })
    specify('cannot redeem less than minRedeemAmount', async function() {
      const minRedeemAmount = await rwat.minRedeemAmount()
      const redeemCall = rwat.connect(whitelisted).redeem(minRedeemAmount - 1)
      await expect(redeemCall).to.be.revertedWith(REVERT_MINIMUM_REDEEM_AMOUNT)
    })
    describe('updating minRedeemAmount', async function() {
      specify('REDEEM_ADMIN_ROLE can update the minRedeemAmount', async function() {
        const ownerIsRedeemAdmin = await rwat.hasRole(REDEEM_ADMIN_ROLE, owner.address)
        expect(ownerIsRedeemAdmin).to.be.true

        const before = await rwat.minRedeemAmount()
        
        const newValue = before + 100

        const updateCall = rwat.updateMinRedeemAmount(newValue)
        await expect(updateCall).to.not.be.reverted

        const after = await rwat.minRedeemAmount()
        expect(after).to.equal(newValue)
      })
      specify('non REDEEM_ADMIN_ROLE can NOT update the minRedeemAmount', async function() {
        const addr1IsRedeemAdmin = await rwat.hasRole(REDEEM_ADMIN_ROLE, addr1.address)
        expect(addr1IsRedeemAdmin).to.be.false

        const updateCall = rwat.connect(addr1).updateMinRedeemAmount(100)
        await expect(updateCall).to.be.revertedWith(REVERT_MISSING_ROLE_REDEEM_ADMIN)
      })
    })
    describe('handling dust', async function() {
      specify('dust remains in users wallet', async function() {
        const balanceBefore = await rwat.balanceOf(whitelisted.address)
        const redeemAmountWithDust = 1_23_4567
        const amountWithoutDust = 1_23_0000
        const redeemCall = rwat.connect(whitelisted).redeem(redeemAmountWithDust)
        await expect(redeemCall).to.not.be.reverted
        await expect(redeemCall).to.emit(rwat, 'Redeem').withArgs(whitelisted.address, amountWithoutDust)
        
        const balanceAfter = await rwat.balanceOf(whitelisted.address)
        expect(balanceAfter).to.equal(balanceBefore - amountWithoutDust)
      })
      specify('REDEEM_ADMIN_ROLE can update dustDecimals', async function() {
        const ownerIsRedeemAdmin = await rwat.hasRole(REDEEM_ADMIN_ROLE, owner.address)
        expect(ownerIsRedeemAdmin).to.be.true

        const updateCall = rwat.updateDustDecimals(2)
        await expect(updateCall).to.be.not.be.reverted
      })
      specify('non REDEEM_ADMIN_ROLE cannot update dustDecimals', async function() {
        const addr1IsRedeemAdmin = await rwat.hasRole(REDEEM_ADMIN_ROLE, addr1.address)
        expect(addr1IsRedeemAdmin).to.be.false

        const updateCall = rwat.connect(addr1).updateDustDecimals(2)
        await expect(updateCall).to.be.revertedWith(REVERT_MISSING_ROLE_REDEEM_ADMIN)
      })
    })
  })

  describe('Oracle Feeds', async function() {
    describe('Updating the oracle contract addresses', async function() {
      beforeEach(async function() {
        await rwat.grantRole(ORACLE_UPDATER_ROLE, owner.address)
      })
      describe('offchainTreasuryOracle', async function() {
        specify('reverts when called without ORACLE_UPDATER_ROLE', async function() {
          const NONoracleUpdater = addr1
          const isOracleUpdater = await rwat.hasRole(ORACLE_UPDATER_ROLE, NONoracleUpdater.address)
          expect(isOracleUpdater).to.be.false
          
          const NONoracleUpdaterCall = rwat.connect(NONoracleUpdater).updateOffchainTreasury(addr2.address)
          await expect(NONoracleUpdaterCall).to.be.revertedWith(REVERT_ADDR1_NOT_ORACLE_UPDATER_ROLE)
        })
        specify('succeeds when called with ORACLE_UPDATE_ROLE', async function() {
          const oracleUpdater = owner
          const isOracleUpdater = await rwat.hasRole(ORACLE_UPDATER_ROLE, oracleUpdater.address)
          expect(isOracleUpdater).to.be.true
          
          const oracleUpdaterCall = rwat.connect(oracleUpdater).updateOffchainTreasury(addr2.address)
          await expect(oracleUpdaterCall).to.not.be.reverted
        })
      })
    })

  })
    
  describe('Asset collateralization', async function() {
    beforeEach(async function() {
      await rwat.updateOffchainTreasury(mockOffchainTreasuryOracle.address)
    })
    specify('can mint while collateralization > 100%', async function() {
      const offchainTreasuryBalance = 1000
      await mockOffchainTreasuryOracle.setBalance(offchainTreasuryBalance)
      const treasuryBalance = await mockOffchainTreasuryOracle.getLatestAnswer()
      expect(treasuryBalance).to.equal(offchainTreasuryBalance)

      const totalPendingRedeems = await rwat.totalPendingRedeems()
      expect(totalPendingRedeems).to.equal(0)

      const balanceBefore = await rwat.balanceOf(whitelisted.address)

      const amountToMint = offchainTreasuryBalance - 1
      const mintCall = mockOffchainTreasuryOracle.passthroughMint(amountToMint, whitelisted.address, CEFI_TX_ID01)
      await expect(mintCall).to.not.be.reverted

      const balanceAfter = await rwat.balanceOf(whitelisted.address)
      expect(balanceAfter).to.equal(balanceBefore + amountToMint)
    })
    specify('can mint up to 100% collateralization', async function() {
      const offchainTreasuryBalance = 1000
      await mockOffchainTreasuryOracle.setBalance(offchainTreasuryBalance)
      const treasuryBalance = await mockOffchainTreasuryOracle.getLatestAnswer()
      expect(treasuryBalance).to.equal(offchainTreasuryBalance)

      const totalPendingRedeems = await rwat.totalPendingRedeems()
      expect(totalPendingRedeems).to.equal(0)

      const balanceBefore = await rwat.balanceOf(whitelisted.address)

      const amountToMint = offchainTreasuryBalance
      const mintCall = mockOffchainTreasuryOracle.passthroughMint(amountToMint, whitelisted.address, CEFI_TX_ID01)
      await expect(mintCall).to.not.be.reverted

      const balanceAfter = await rwat.balanceOf(whitelisted.address)
      expect(balanceAfter).to.equal(balanceBefore + amountToMint)
    })
    specify('reverts when minting would result in < 100% collateralization', async function() {
      const offchainTreasuryBalance = 1000
      await mockOffchainTreasuryOracle.setBalance(offchainTreasuryBalance)
      const treasuryBalance = await mockOffchainTreasuryOracle.getLatestAnswer()
      expect(treasuryBalance).to.equal(offchainTreasuryBalance)

      const totalPendingRedeems = await rwat.totalPendingRedeems()
      expect(totalPendingRedeems).to.equal(0)

      const amountToMint = offchainTreasuryBalance + 1
      const mintCall = mockOffchainTreasuryOracle.passthroughMint(amountToMint, whitelisted.address, CEFI_TX_ID01)
      await expect(mintCall).to.be.revertedWith(REVERT_SUPPLY_CANNOT_EXCEED_ASSETS)
    })
  })

  describe("OffchainTreasury", async function() {
    specify('returns the latest balance', async function() {
      const balance = await offchainTreasury.getLatestAnswer()
      expect(balance).to.equal(0)
    })
    specify('reverts if mint is called by non-treasury account', async function() {
      const mintCall = rwat.mint(addr2.address, 0, randomBytes32());
      await expect(mintCall).to.be.revertedWith(REVERT_MINT_CALL_NONTREASURY)
    })
    specify('reverts if releasePendingRedeem is called by non-treasury account', async function() {
      const releasePendingRedeemCall = rwat.releasePendingRedeem(addr2.address, 0, randomBytes32());
      await expect(releasePendingRedeemCall).to.be.revertedWith(REVERT_RELEASEREDEEM_CALL_NONTREASURY)
    })
    describe('updating', async function() {
      beforeEach(async () => {
        await rwat.addToWhitelist(addr1.address, signWhitelistAddress(owner, addr1.address))
        await rwat.addToWhitelist(addr2.address, signWhitelistAddress(owner, addr2.address))
      })
      specify('updates the latest balance correctly', async function() {
        const balanceBefore = await offchainTreasury.getLatestAnswer()

        const updateAmounts = [100, 50]
      
        const updateCall = offchainTreasury.update(
          updateAmounts, 
          [addr1.address, addr2.address], 
          [CEFI_TX_ID01, DEFI_TX_ID01],
          [CHAIN_ID_ETHEREUM, CHAIN_ID_ETHEREUM],
          [
            await signUpdateCall(owner, updateAmounts[0], addr1.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM), 
            await signUpdateCall(owner, updateAmounts[1], addr2.address, DEFI_TX_ID01, CHAIN_ID_ETHEREUM),
          ]
        )
        await expect(updateCall).to.not.be.reverted

        const balanceAfter = await offchainTreasury.getLatestAnswer()

        const sumOfUpdates = updateAmounts.reduce((a, b) => a+b)

        expect(balanceAfter).to.equal(+balanceBefore + sumOfUpdates)
      })
      specify('emits an update event for every update', async function() {
        const updateCall = offchainTreasury.update(
          [100, 50], 
          [addr1.address, addr2.address], 
          [CEFI_TX_ID01, CEFI_TX_ID02],
          [CHAIN_ID_ETHEREUM, CHAIN_ID_ETHEREUM],
          [
            await signUpdateCall(owner, 100, addr1.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM), 
            await signUpdateCall(owner, 50, addr2.address, CEFI_TX_ID02, CHAIN_ID_ETHEREUM),
          ]
        )
        await expect(updateCall).to.not.be.reverted
        await expect(updateCall).to.emit(offchainTreasury, 'Inflow').withArgs(CEFI_TX_ID01, 100, CHAIN_ID_ETHEREUM)
        await expect(updateCall).to.emit(offchainTreasury, 'Inflow').withArgs(CEFI_TX_ID02, 50, CHAIN_ID_ETHEREUM)
      })
      specify('emits an TxAmountZero event for an update with amount 0', async function() {
        const updateCall = offchainTreasury.update([0], [addr1.address], [CEFI_TX_ID01], [CHAIN_ID_ETHEREUM], [
          await signUpdateCall(owner, [0], addr1.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM), 
        ])
        await expect(updateCall).to.not.be.reverted
        await expect(updateCall).to.emit(offchainTreasury, 'TxAmountZero').withArgs(CEFI_TX_ID01)
        await expect(updateCall).to.not.emit(offchainTreasury, 'Inflow');
        await expect(updateCall).to.not.emit(offchainTreasury, 'Outflow');
      })
      specify('marks the processed tx as processed', async function() {
        const before = await offchainTreasury.processedCeFiTxIds(CEFI_TX_ID01)
        expect(before).to.be.false

        const updateCall = offchainTreasury.update([100], [addr1.address], [CEFI_TX_ID01], [CHAIN_ID_ETHEREUM], [
          await signUpdateCall(owner, [100], addr1.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM), 
        ])
        await expect(updateCall).to.not.be.reverted

        const after = await offchainTreasury.processedCeFiTxIds(CEFI_TX_ID01)
        expect(after).to.be.true
      })
      specify('if minting fails the tx is not marked as processed', async () => {
        const before = await offchainTreasury.processedCeFiTxIds(CEFI_TX_ID01)
        expect(before).to.be.false

        const updateCall = offchainTreasury.update([100], [NOTwhitelisted.address], [CEFI_TX_ID01], [CHAIN_ID_ETHEREUM], [
          await signUpdateCall(owner, [100], NOTwhitelisted.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM), 
        ])
        await expect(updateCall).to.not.be.reverted

        const after = await offchainTreasury.processedCeFiTxIds(CEFI_TX_ID01)
        expect(after).to.be.false
      })
      specify('if minting fails the balance is not updated', async () => {
        const before = await offchainTreasury.balance()

        const updateCall = offchainTreasury.update([100], [NOTwhitelisted.address], [CEFI_TX_ID01], [CHAIN_ID_ETHEREUM], [
          await signUpdateCall(owner, [100], NOTwhitelisted.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM), 
        ])
        await expect(updateCall).to.not.be.reverted

        const after = await offchainTreasury.balance()
        expect(after).to.equal(before)
      })
      specify('does not process the same TxID twice', async function() {
        const balanceBefore = await offchainTreasury.getLatestAnswer()

        const updateCall = offchainTreasury.update(
          [100, 100], 
          [addr1.address, addr2.address], 
          [CEFI_TX_ID01, CEFI_TX_ID01],
          [CHAIN_ID_ETHEREUM, CHAIN_ID_ETHEREUM],
          [
            await signUpdateCall(owner, 100, addr1.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM), 
            await signUpdateCall(owner, 100, addr2.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM),
          ]
        )
        await expect(updateCall).to.not.be.reverted

        const balanceAfter = await offchainTreasury.getLatestAnswer()
        expect(balanceAfter).to.equal(+balanceBefore + 100)
      })
      specify('emits an TxAlreadyProcessed event if the txID has already been processed', async function() {
        const updateCall = offchainTreasury.update(
          [100, 100], 
          [addr1.address, addr2.address], 
          [CEFI_TX_ID01, CEFI_TX_ID01],
          [CHAIN_ID_ETHEREUM, CHAIN_ID_ETHEREUM],
          [
            await signUpdateCall(owner, 100, addr1.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM), 
            await signUpdateCall(owner, 100, addr2.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM),
          ]
        )
        await expect(updateCall).to.not.be.reverted
        await expect(updateCall).to.emit(offchainTreasury, 'TxAlreadyProcessed').withArgs(CEFI_TX_ID01)
      })
      specify('reverts when invalid arguments are given', async function() {
        const invalidAmountArgumentCall = offchainTreasury.update(
          [100], 
          [addr1.address, addr2.address], 
          [CEFI_TX_ID01, CEFI_TX_ID02],
          [CHAIN_ID_ETHEREUM, CHAIN_ID_ETHEREUM],
          ['0x', '0x']
        )
        await expect(invalidAmountArgumentCall).to.be.revertedWith(REVERT_INVALID_UPDATE_ARGUMENTS)

        const invalidAddrArgumentCall = offchainTreasury.update(
          [100, 200], 
          [addr1.address], 
          [CEFI_TX_ID01, CEFI_TX_ID02],
          [CHAIN_ID_ETHEREUM, CHAIN_ID_ETHEREUM],
          ['0x', '0x']
        )
        await expect(invalidAddrArgumentCall).to.be.revertedWith(REVERT_INVALID_UPDATE_ARGUMENTS)

        const invalidTxIDArgumentCall = offchainTreasury.update(
          [100, 200], 
          [addr1.address, addr2.address], 
          [CEFI_TX_ID01],
          [CHAIN_ID_ETHEREUM, CHAIN_ID_ETHEREUM],
          ['0x', '0x'])
          
        await expect(invalidTxIDArgumentCall).to.be.revertedWith(REVERT_INVALID_UPDATE_ARGUMENTS)

        const invalidChainIDArgumentCall = offchainTreasury.update(
          [100, 200], 
          [addr1.address, addr2.address], 
          [CEFI_TX_ID01, CEFI_TX_ID02],
          [CHAIN_ID_ETHEREUM],
          ['0x', '0x'])
        await expect(invalidChainIDArgumentCall).to.be.revertedWith(REVERT_INVALID_UPDATE_ARGUMENTS)
        
        const invalidSignatureArgumentCall = offchainTreasury.update(
          [100, 200], 
          [addr1.address, addr2.address], 
          [CEFI_TX_ID01, CEFI_TX_ID02],
          [CHAIN_ID_ETHEREUM],
          ['0x'])
        await expect(invalidSignatureArgumentCall).to.be.revertedWith(REVERT_INVALID_UPDATE_ARGUMENTS)
      })
      specify('calls the rwatTokenContract and mints the tokens for new inflows', async function() {
        const balanceBefore = await rwat.balanceOf(whitelisted.address)

        const updateCall = offchainTreasury.update([100], [whitelisted.address], [CEFI_TX_ID01], [CHAIN_ID_ETHEREUM], [
          await signUpdateCall(owner, [100], whitelisted.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM), 
        ])
        await expect(updateCall).to.not.be.reverted

        await expect(updateCall).to.emit(rwat, 'Mint').withArgs(whitelisted.address, 100, CEFI_TX_ID01)

        const balanceAfter = await rwat.balanceOf(whitelisted.address)
        expect(balanceAfter).to.equal(balanceBefore + 100)
      })
      specify('will not mint tokens for new inflows on the wrong chain', async function() {
        const balanceBefore = await rwat.balanceOf(whitelisted.address)

        const updateCall = offchainTreasury.update([100], [whitelisted.address], [CEFI_TX_ID01], [CHAIN_ID_POLYGON], [
          await signUpdateCall(owner, [100], whitelisted.address, CEFI_TX_ID01, CHAIN_ID_POLYGON), 
        ])
        await expect(updateCall).to.not.be.reverted
        await expect(updateCall).to.emit(offchainTreasury, 'Inflow')
        await expect(updateCall).to.not.emit(rwat, 'Mint')

        const balanceAfter = await rwat.balanceOf(whitelisted.address)
        expect(balanceAfter).to.equal(balanceBefore)
      })
      specify('calls the rwatTokenContract and releases pending for new outflows', async function() {
        const updateCall = offchainTreasury.update([100], [whitelisted.address], [CEFI_TX_ID01], [CHAIN_ID_ETHEREUM], [
          await signUpdateCall(owner, [100], whitelisted.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM), 
        ])
        await expect(updateCall).to.not.be.reverted
        
        const balanceBefore = await rwat.balanceOf(whitelisted.address)
        expect(balanceBefore).to.equal(100)
        
        const redeemCall = rwat.connect(whitelisted).redeem(50);
        await expect(redeemCall).to.not.be.reverted
        
        const balanceAfter = await rwat.balanceOf(whitelisted.address)
        expect(balanceAfter).to.equal(50)
        
        const totalPendingBefore = await rwat.totalPendingRedeems()
        expect(totalPendingBefore).to.equal(50)
        
        const randomBytes = randomBytes32()
        const outflowUpdateCall = offchainTreasury.update([-50], [whitelisted.address], [randomBytes], [CHAIN_ID_ETHEREUM], [
          await signUpdateCall(owner, [-50], whitelisted.address, randomBytes, CHAIN_ID_ETHEREUM), 
        ])
        await expect(outflowUpdateCall).to.not.be.reverted
        await expect(outflowUpdateCall).to.emit(rwat, 'PendingRedeemReleased').withArgs(whitelisted.address, 50, randomBytes)
        
        const totalPendingAfter = await rwat.totalPendingRedeems()
        expect(totalPendingAfter).to.equal(0)
      })
      specify('will not attempt to releases pending for new outflows on the wrong chain', async function() {
        const updateCall = offchainTreasury.update([100], [whitelisted.address], [CEFI_TX_ID01], [CHAIN_ID_ETHEREUM], [
          await signUpdateCall(owner, [100], whitelisted.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM), 
        ])
        await expect(updateCall).to.not.be.reverted
        
        const balanceBefore = await rwat.balanceOf(whitelisted.address)
        expect(balanceBefore).to.equal(100)
        
        const redeemCall = rwat.connect(whitelisted).redeem(50);
        await expect(redeemCall).to.not.be.reverted
        
        const balanceAfter = await rwat.balanceOf(whitelisted.address)
        expect(balanceAfter).to.equal(50)
        
        const totalPendingBefore = await rwat.totalPendingRedeems()
        expect(totalPendingBefore).to.equal(50)
        
        const randomBytes = randomBytes32()
        const outflowUpdateCall = offchainTreasury.update([-50], [whitelisted.address], [randomBytes], [CHAIN_ID_POLYGON], [
          await signUpdateCall(owner, [-50], whitelisted.address, randomBytes, CHAIN_ID_POLYGON), 
        ])
        await expect(outflowUpdateCall).to.not.be.reverted
        await expect(outflowUpdateCall).to.emit(offchainTreasury, 'Outflow')
        await expect(outflowUpdateCall).to.not.emit(rwat, 'PendingRedeemReleased')
        
        const totalPendingAfter = await rwat.totalPendingRedeems()
        expect(totalPendingAfter).to.equal(totalPendingBefore)
      })
      specify('will skip releasing a redeem if it exceeds the pending redeem amount', async function() {
        const updateCall = offchainTreasury.update([100], [whitelisted.address], [CEFI_TX_ID01], [CHAIN_ID_ETHEREUM], [
          await signUpdateCall(owner, [100], whitelisted.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM), 
        ])
        await expect(updateCall).to.not.be.reverted
        
        const balanceBefore = await rwat.balanceOf(whitelisted.address)
        expect(balanceBefore).to.equal(100)
        
        const redeemCall = rwat.connect(whitelisted).redeem(50);
        await expect(redeemCall).to.not.be.reverted
        
        const balanceAfter = await rwat.balanceOf(whitelisted.address)
        expect(balanceAfter).to.equal(50)
        
        const totalPendingBefore = await rwat.totalPendingRedeems()
        expect(totalPendingBefore).to.equal(50)
        
        const randomBytes = randomBytes32()
        const outflowUpdateCall = offchainTreasury.update([-200], [whitelisted.address], [randomBytes], [CHAIN_ID_ETHEREUM], [
          await signUpdateCall(owner, [-200], whitelisted.address, randomBytes, CHAIN_ID_ETHEREUM), 
        ])
        await expect(outflowUpdateCall).to.not.be.reverted
        await expect(outflowUpdateCall).to.not.emit(rwat, 'PendingRedeemReleased')
        await expect(outflowUpdateCall).to.emit(rwat, 'TxRedeemInvalidPendingAmount').withArgs(randomBytes)
        
        const totalPendingAfter = await rwat.totalPendingRedeems()
        expect(totalPendingAfter).to.equal(totalPendingBefore)
      })
      specify('will skip and emit a TxInvalidSignature event when signature is invalid', async function() {
        const updateCall = offchainTreasury.update([100], [whitelisted.address], [CEFI_TX_ID01], [CHAIN_ID_ETHEREUM], [
          await signUpdateCall(addr1, [100], whitelisted.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM), 
        ])
        await expect(updateCall).to.not.be.reverted
        await expect(updateCall).to.emit(offchainTreasury, 'TxInvalidSignature')
      })
      specify('will not update balance if pending redeem is not released', async function() {
        const mintCall = offchainTreasury.update([100], [whitelisted.address], [CEFI_TX_ID01], [CHAIN_ID_ETHEREUM], [
          await signUpdateCall(owner, [100], whitelisted.address, CEFI_TX_ID01, CHAIN_ID_ETHEREUM), 
        ])
        await expect(mintCall).to.not.be.reverted
        
        const b = await rwat.balanceOf(whitelisted.address)
        expect(b).to.equal(100)

        const redeemCall = rwat.connect(whitelisted).redeem(50);
        await expect(redeemCall).to.not.be.reverted

        const balanceBefore = await offchainTreasury.balance()

        const updateCall = offchainTreasury.update([-100], [whitelisted.address], [CEFI_TX_ID02], [CHAIN_ID_ETHEREUM], [
          await signUpdateCall(owner, [-100], whitelisted.address, CEFI_TX_ID02, CHAIN_ID_ETHEREUM), 
        ])
        await expect(updateCall).to.not.be.reverted
        await expect(updateCall).to.emit(rwat, 'TxRedeemInvalidPendingAmount')

        const balanceAfter = await offchainTreasury.balance() 
        expect(balanceAfter).to.equal(balanceBefore)
      })
    })
    describe('updating token contract', async function() {
      specify('reverts when passing the zero address', async function() {
        const updateCall = offchainTreasury.updateTokenContract(ZERO_ADDRESS)
        await expect(updateCall).to.be.revertedWith(REVERT_TOKEN_CONTRACT_ZERO_ADDRESS)
      })
      specify('updates the token contract', async function() {
        const before = await offchainTreasury.tokenContract()
        expect(before).to.equal(rwat.address)

        const dummyAddress = addr1.address

        const updateCall = offchainTreasury.updateTokenContract(dummyAddress)
        await expect(updateCall).to.not.be.reverted

        const after = await offchainTreasury.tokenContract()
        expect(after).to.equal(dummyAddress)
      })
    })
    describe('updating offchain signer', async function() {
      specify('updates the offchainSigner', async function() {
        const before = await offchainTreasury.offchainSigner()
        expect(before).to.equal(owner.address)
        
        const newOffchainSigner = addr1.address
        
        const updateCall = offchainTreasury.updateOffchainSigner(newOffchainSigner)
        await expect(updateCall).to.not.be.reverted
        
        const after = await offchainTreasury.offchainSigner()
        expect(after).to.equal(newOffchainSigner)
      })
      specify('reverts when passing the zero address', async function() {
        const updateCall = offchainTreasury.updateOffchainSigner(ZERO_ADDRESS)
        await expect(updateCall).to.be.revertedWith(REVERT_OFFCHAIN_SIGNER_ZERO_ADDRESS)
      })
      specify('reverts when new signer is same as old signer', async function() {
        const updateCall = offchainTreasury.updateOffchainSigner(owner.address)
        await expect(updateCall).to.be.revertedWith(REVERT_OFFCHAIN_SIGNER_EQUALS_CURRENT)
      })
    })
    specify('cannot be deployed with zero address as signer', async function() {
      await expect(OffchainTreasuryFactory.deploy(ZERO_ADDRESS)).to.be.revertedWith(REVERT_OFFCHAIN_SIGNER_ZERO_ADDRESS)
    })
  })

  describe('Fees', async function() {
    describe('mint fees', async function() {
      const mintFee = 100 // 1.00%
      beforeEach(async function() {
        rwat.updateOffchainTreasury(mockOffchainTreasuryOracle.address)
        rwat.updateMintFee(mintFee)
        mockOffchainTreasuryOracle.setBalance(100_000*10**RWAT_DECIMALS)
      })
      specify('a fee is deducted during mint',async function() {
        const balanceBefore = await rwat.balanceOf(whitelisted.address)

        const amount = 100*10**RWAT_DECIMALS
        const mintCall = mockOffchainTreasuryOracle.passthroughMint(amount, whitelisted.address, randomBytes32())
        await expect(mintCall).to.not.be.reverted

        const balanceAfter = await rwat.balanceOf(whitelisted.address)
        expect(balanceAfter).to.equal(+balanceBefore + amount*0.99)
      })
      specify('the fee is received by the feeReceiver', async function() {
        const balanceBefore = await rwat.balanceOf(owner.address)

        const amount = 100*10**RWAT_DECIMALS
        const expectedFee = amount*0.01
        const mintCall = mockOffchainTreasuryOracle.passthroughMint(amount, whitelisted.address, randomBytes32())
        await expect(mintCall).to.not.be.reverted
        await expect(mintCall).to.emit(rwat, 'Transfer').withArgs(ADDRESS_ZERO, owner.address, expectedFee)

        const balanceAfter = await rwat.balanceOf(owner.address)
        expect(balanceAfter).to.equal(+balanceBefore + expectedFee)
      })
      specify('FEE_ADMIN_ROLE can update mintFee', async function() {
        const ownerIsFeeAdmin = await rwat.hasRole(FEE_ADMIN_ROLE, owner.address)
        expect(ownerIsFeeAdmin).to.be.true

        const feeBefore = await rwat.mintFee()

        const newFee = feeBefore.add(100);
        
        const updateCall = rwat.updateMintFee(newFee)
        await expect(updateCall).to.not.be.reverted
        await expect(updateCall).to.emit(rwat, 'MintFeeUpdated').withArgs(newFee)

        const feeAfter = await rwat.mintFee()
        expect(feeAfter).to.equal(newFee)
      })
      specify('non FEE_ADMIN_ROLE cannot update mintFee', async function() {
        const addr1IsFeeAdmin = await rwat.hasRole(FEE_ADMIN_ROLE, addr1.address)
        expect(addr1IsFeeAdmin).to.be.false
        const updateCall = rwat.connect(addr1).updateMintFee(1234)
        await expect(updateCall).to.be.revertedWith(REVERT_MISSING_ROLE_FEE_ADMIN)
      })
    })
    describe('redeem fees', async function() {
      const redeemFee = 200 // 2.00%
      beforeEach(async function() {
        rwat.updateOffchainTreasury(mockOffchainTreasuryOracle.address)
        rwat.updateRedeemFee(redeemFee)
        mockOffchainTreasuryOracle.setBalance(100_000*10**RWAT_DECIMALS)
        const mintAmount = 100*10**RWAT_DECIMALS
        const mintCall = mockOffchainTreasuryOracle.passthroughMint(mintAmount, whitelisted.address, randomBytes32())
        await expect(mintCall).to.not.be.reverted
      })
      specify('a fee is deducted during redeem',async function() {
        const balanceBefore = await rwat.balanceOf(whitelisted.address)

        const redeemAmount = 10*10**RWAT_DECIMALS
        const redeemCall = rwat.connect(whitelisted).redeem(redeemAmount)
        await expect(redeemCall).to.not.be.reverted
        await expect(redeemCall).to.emit(rwat, 'Redeem').withArgs(whitelisted.address, redeemAmount*0.98)

        const balanceAfter = await rwat.balanceOf(whitelisted.address)
        expect(balanceAfter).to.equal(+balanceBefore - redeemAmount)
      })
      specify('the fee is received by the feeReceiver', async function() {
        const balanceBefore = await rwat.balanceOf(owner.address)

        const redeemAmount = 10*10**RWAT_DECIMALS
        const redeemCall = rwat.connect(whitelisted).redeem(redeemAmount)
        await expect(redeemCall).to.not.be.reverted
        await expect(redeemCall).to.emit(rwat, 'Transfer').withArgs(whitelisted.address, owner.address, redeemAmount*0.02)

        const balanceAfter = await rwat.balanceOf(owner.address)
        expect(balanceAfter).to.equal(+balanceBefore + redeemAmount*0.02)
      })
      specify('FEE_ADMIN_ROLE can update redeemFee', async function() {
        const ownerIsFeeAdmin = await rwat.hasRole(FEE_ADMIN_ROLE, owner.address)
        expect(ownerIsFeeAdmin).to.be.true

        const feeBefore = await rwat.redeemFee()

        const newFee = feeBefore.add(100)

        const updateCall = rwat.updateRedeemFee(newFee)
        await expect(updateCall).to.not.be.reverted
        await expect(updateCall).to.emit(rwat, 'RedeemFeeUpdated').withArgs(newFee)

        const feeAfter = await rwat.redeemFee()
        expect(feeAfter).to.equal(newFee)
      })
      specify('non FEE_ADMIN_ROLE cannot update redeemFee', async function() {
        const addr1IsFeeAdmin = await rwat.hasRole(FEE_ADMIN_ROLE, addr1.address)
        expect(addr1IsFeeAdmin).to.be.false
        const updateCall = rwat.connect(addr1).updateRedeemFee(1234)
        await expect(updateCall).to.be.revertedWith(REVERT_MISSING_ROLE_FEE_ADMIN)
      })
    })
    describe('feeReceiver', async function() {
      specify('FEE_ADMIN_ROLE can update feeReceiver', async function() {
        const ownerIsFeeAdmin = await rwat.hasRole(FEE_ADMIN_ROLE, owner.address)
        expect(ownerIsFeeAdmin).to.be.true

        const feeReceiverBefore = await rwat.feeReceiver()
        expect(feeReceiverBefore).to.not.equal(addr1.address)

        const updateCall = rwat.updateFeeReceiver(addr1.address)
        await expect(updateCall).to.not.be.reverted

        const feeReceiverAfter = await rwat.feeReceiver()
        expect(feeReceiverAfter).to.equal(addr1.address)
      })
      specify('non FEE_ADMIN_ROLE cannot update feeReceiver', async function() {
        const addr1IsFeeAdmin = await rwat.hasRole(FEE_ADMIN_ROLE, addr1.address)
        expect(addr1IsFeeAdmin).to.be.false

        const updateCall = rwat.connect(addr1).updateFeeReceiver(addr1.address)
        await expect(updateCall).to.be.revertedWith(REVERT_MISSING_ROLE_FEE_ADMIN)
      })
      specify('cannot update to to same as current', async function() {
        const ownerIsFeeAdmin = await rwat.hasRole(FEE_ADMIN_ROLE, owner.address)
        expect(ownerIsFeeAdmin).to.be.true

        const feeReceiverBefore = await rwat.feeReceiver()

        const updateCall = rwat.updateFeeReceiver(feeReceiverBefore)
        await expect(updateCall).to.be.revertedWith(REVERT_SAME_FEE_RECEIVER)
      })
    })
  })
  
  describe('orcale configuration', () => {
    describe('updateOffchainTreasury()', () => {
      specify('reverts if called by account without ORACLE_UPDATER_ROLE', async function() {
        const updateCall = rwat.connect(addr1).updateOffchainTreasury(addr2.address)
        const REVERT_UPDATETREASURYORACLE_INVALIDROLE = 'AccessControl: account 0x70997970c51812dc3a010c7d01b50e0d17dc79c8 is missing role 0x9792fdc19ab98adfa72ab2fa98d342618c661e01c406979c105b31eda87f5e6f'
        await expect(updateCall).to.be.revertedWith(REVERT_UPDATETREASURYORACLE_INVALIDROLE)
      })
      specify('reverts if new address is address zero', async function() {
        const updateCall = rwat.updateOffchainTreasury(ADDRESS_ZERO)
        const REVERT_UPDATETREASURYORACLE_ADDRESSZERO = '_newOracleAddress is 0x0'
        await expect(updateCall).to.be.revertedWith(REVERT_UPDATETREASURYORACLE_ADDRESSZERO)
      })
      specify('reverts if new address equals current address', async function() {
        const updateCall = rwat.updateOffchainTreasury(offchainTreasury.address)
        const REVERT_UPDATETREASURYORACLE_EQUALSEXISTING = '_newOracleAddress equals current'
        await expect(updateCall).to.be.revertedWith(REVERT_UPDATETREASURYORACLE_EQUALSEXISTING)
      })
      specify('emits UpdateOffchainTreasuryOracle event and sets offchainTreasuryOracle', async function() {
        const updateCall = rwat.updateOffchainTreasury(addr2.address)
        await expect(updateCall).to.emit(rwat, 'UpdateOffchainTreasuryOracle').withArgs(addr2.address)
        expect(await rwat.offchainTreasuryOracle()).to.equal(addr2.address)
      })
    });
  })
  
  describe('fee configuration', () => {
    describe('updateMintFee()', () => {
      specify('reverts if called by account without FEE_ADMIN_ROLE', async function() {
        const updateCall = rwat.connect(addr1).updateMintFee(5000)
        await expect(updateCall).to.be.revertedWith('AccessControl: account 0x70997970c51812dc3a010c7d01b50e0d17dc79c8 is missing role 0x349eec46d2c564a125684d1934a6120a70cc341d3a26d362e358a775e07dec17')
      })
      specify('reverts if new fee equals current fee', async function() {
        const updateCall = rwat.updateMintFee(0)
        await expect(updateCall).to.be.revertedWith('_mintFee equals current')
      })
      specify('reverts if new fee above 100%', async function() {
        const updateCall = rwat.updateMintFee(10001)
        await expect(updateCall).to.be.revertedWith('_mintFee exceeds 100%')
      })
      specify('emits MintFeeUpdated event and sets mintFee', async function() {
        const updateCall = rwat.updateMintFee(2500)
        await expect(updateCall).to.emit(rwat, 'MintFeeUpdated').withArgs(2500)
        expect(await rwat.mintFee()).to.equal(2500)
      })
    });
    describe('updateRedeemFee()', () => {
      specify('reverts if called by account without FEE_ADMIN_ROLE', async function() {
        const updateCall = rwat.connect(addr1).updateRedeemFee(5000)
        await expect(updateCall).to.be.revertedWith('AccessControl: account 0x70997970c51812dc3a010c7d01b50e0d17dc79c8 is missing role 0x349eec46d2c564a125684d1934a6120a70cc341d3a26d362e358a775e07dec17')
      })
      specify('reverts if new fee equals current fee', async function() {
        const updateCall = rwat.updateRedeemFee(0)
        await expect(updateCall).to.be.revertedWith('_redeemFee equals current')
      })
      specify('reverts if new fee above 100%', async function() {
        const updateCall = rwat.updateRedeemFee(10001)
        await expect(updateCall).to.be.revertedWith('_redeemFee exceeds 100%')
      })
      specify('emits RedeemFeeUpdated event and sets redeemFee', async function() {
        const updateCall = rwat.updateRedeemFee(2500)
        await expect(updateCall).to.emit(rwat, 'RedeemFeeUpdated').withArgs(2500)
        expect(await rwat.redeemFee()).to.equal(2500)
      })
    });
    describe('updateFeeReceiver()', () => {
      specify('reverts if called by account without FEE_ADMIN_ROLE', async function() {
        const updateCall = rwat.connect(addr1).updateFeeReceiver(addr2.address)
        await expect(updateCall).to.be.revertedWith('AccessControl: account 0x70997970c51812dc3a010c7d01b50e0d17dc79c8 is missing role 0x349eec46d2c564a125684d1934a6120a70cc341d3a26d362e358a775e07dec17')
      })
      specify('reverts if new address is address zero', async function() {
        const updateCall = rwat.updateFeeReceiver(ADDRESS_ZERO)
        await expect(updateCall).to.be.revertedWith('_feeReceiver is 0x0')
      })
      specify('reverts if new address equals current address', async function() {
        const updateCall = rwat.updateFeeReceiver(owner.address)
        await expect(updateCall).to.be.revertedWith('_feeReceiver equals current')
      })
      specify('emits FeeReceiverUpdated event and sets feeReceiver', async function() {
        const updateCall = rwat.updateFeeReceiver(addr2.address)
        await expect(updateCall).to.emit(rwat, 'FeeReceiverUpdated').withArgs(addr2.address)
        expect(await rwat.feeReceiver()).to.equal(addr2.address)
      })
    });
  });
  describe('redeem configuration', () => {
    describe('updateMinRedeemAmount()', () => {
      specify('reverts if called by account without REDEEM_ADMIN_ROLE', async function() {
        const updateCall = rwat.connect(addr1).updateMinRedeemAmount(5000)
        await expect(updateCall).to.be.revertedWith('AccessControl: account 0x70997970c51812dc3a010c7d01b50e0d17dc79c8 is missing role 0x281081d9b36b37208f0d8dfce5adc7e00d31ece09269aaa8d0bfa43e6840a338')
      })
      specify('reverts if new min redeem amount equals current min redeem amount', async function() {
        const updateCall = rwat.updateMinRedeemAmount(1)
        await expect(updateCall).to.be.revertedWith('_minRedeemAmount equals current')
      })
      specify('emits MinRedeemAmountUpdated event and sets minRedeemAmount', async function() {
        const updateCall = rwat.updateMinRedeemAmount(2500)
        await expect(updateCall).to.emit(rwat, 'MinRedeemAmountUpdated').withArgs(2500)
        expect(await rwat.minRedeemAmount()).to.equal(2500)
      })
    });
    describe('updateDustDecimals()', () => {
      specify('reverts if called by account without REDEEM_ADMIN_ROLE', async function() {
        const updateCall = rwat.connect(addr1).updateDustDecimals(5)
        await expect(updateCall).to.be.revertedWith('AccessControl: account 0x70997970c51812dc3a010c7d01b50e0d17dc79c8 is missing role 0x281081d9b36b37208f0d8dfce5adc7e00d31ece09269aaa8d0bfa43e6840a338')
      })
      specify('reverts if new dust decimals equals current dust decimals', async function() {
        const updateCall = rwat.updateDustDecimals(0)
        await expect(updateCall).to.be.revertedWith('_dustDecimals equals current')
      })
      specify('reverts if new dust decimals larger than DECIMALS_VALUE', async function() {
        const updateCall = rwat.updateDustDecimals(7)
        await expect(updateCall).to.be.revertedWith('_dustDecimals too big')
      })
      specify('emits DustDecimalsUpdated event and sets dustDecimals', async function() {
        const updateCall = rwat.updateDustDecimals(5)
        await expect(updateCall).to.emit(rwat, 'DustDecimalsUpdated').withArgs(5)
        expect(await rwat.dustDecimals()).to.equal(5)
      })
    });
  });
})
